const express = require("express");
const app = express();
const path = require("path");
const mustache = require("mustache-express");
const mongoose = require("mongoose");
const routeAbout = require("./routes/about");
const routeIndex = require("./routes/index");
const routeEvents = require("./routes/events");
const routeCalendars = require("./routes/calendars");
const config = require("./config");
const flash = require('connect-flash');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const morgan = require('morgan'); //
const bodyParser = require('body-parser');//
const passport = require('passport');
module.exports = passport;


app.use(express.static("public"));


const viewsDir = path.join(__dirname, "views");
const partialDir = path.join(viewsDir, "partials");
app.engine("mst", mustache(partialDir));
app.set("views", viewsDir);
app.set("view engine", "mst");



app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('dev')); 
app.use(cookieParser());
// app.use(bodyParser());

require('./passport')(passport); 

app.use(session({
  secret: "Some_secret_string",
	resave: false,
	saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session()); 
app.use(flash()); 



require('./routes/auth')(app, passport);

const routeApi = require('./routes/api');
const routeDeveloper = require("./routes/developer");

app.use("/", routeIndex);
// app.use("/", routeUsers);
app.use("/", routeEvents);
app.use("/", routeCalendars);
app.use("/", routeAbout);
app.use('/api/v1', routeApi);
app.use('/developer/v1', routeDeveloper);


require('./routes/error')(app);



const databaseUrl = config.DatabaseUrl;
const serverPort = config.ServerPort;

const connectOptions = { useNewUrlParser: true, useUnifiedTopology: true };

mongoose
.connect(databaseUrl, connectOptions)
.then(() => console.log(`Mongo database connected: ${databaseUrl}`))
.then(() =>app.listen(serverPort, () => console.log(`Server started: ${serverPort}`)))
.catch(err => console.log(`Start error: ${err}`));
