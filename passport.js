var LocalStrategy = require('passport-local').Strategy;
var User = require('./models/user');
var BasicStrategy = require('passport-http').BasicStrategy;
var mongoose = require('mongoose');

module.exports = function (passport) {

    passport.use('basic', new BasicStrategy(
        function (login, password, done) {
            User.findOne({ login: login }, function (err, user) {
                if (err) { return done(err); }
                if (!user) { return done(null, false); }
                if (!user.validPassword(password)) { return done(null, false); }
                return done(null, user);
            });
        }
    ));

    passport.serializeUser(function (user, done) {
        done(null, user._id);
    });

    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            done(err, user);
        });
    });


    passport.use('local-signup', new LocalStrategy({
        usernameField: 'login',
        passwordField: 'password',
        passReqToCallback: true
    },
        function (req, log, password, done) {
            process.nextTick(function () {
                User.findOne({ 'login': log }, function (err, user) {
                    if (err)
                        return done(err);
                        if (user && !user.isDisabled) {
                            return done(null, false, req.flash('signupMessage', 'Login is used'));
                        }
                    else if (req.body.password !== req.body.password2)
                        return done(null, false, req.flash('signupMessage', 'Passwords do not match'));
                    else if (req.body.password.length < 4)
                        return done(null, false, req.flash('signupMessage', 'Too easy password'));
                    else if (req.body.login.length < 3)
                        return done(null, false, req.flash('signupMessage', 'Too short login'));
                    else if (req.body.fullname.length < 2)
                        return done(null, false, req.flash('signupMessage', 'Too short fullname'));
                    else {
                        let newUser = new User();

                        newUser._id = new mongoose.Types.ObjectId();
                        newUser.login = log;
                        newUser.password = newUser.generateHash(password);
                        newUser.fullname = req.body.fullname;
                        newUser.biography = '';
                        newUser.role = 0;
                        newUser.registeredAt = new Date().toISOString();
                        newUser.avaUrl = "https://res.cloudinary.com/hmgldm3w6/image/upload/v1574987203/qhdzojllhx0ip2shjmav.gif";
                        newUser.isDisabled = false;
                        newUser.eventsId = [];
                        newUser.calendarsId = [];

                        newUser.save(function (err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });
                    }

                });

            });

        }));


    passport.use('local-login', new LocalStrategy({
        usernameField: 'login',
        passwordField: 'password',
        passReqToCallback: true
    },
        function (req, login, password, done) {
            User.findOne({ 'login': login, 'isDisabled': false }, function (err, user) {
                if (err)
                    return done(err);

                if (!user)
                    return done(null, false, req.flash('loginMessage', 'Wrong login')); 

                if (!user.validPassword(password))
                    return done(null, false, req.flash('loginMessage', 'Wrong password'));

                return done(null, user);
            });

        }));

};