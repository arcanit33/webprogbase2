var curPage = document.getElementById("page");
var lastPage = document.getElementById("lastPage");
var searchInput = document.getElementById("searchInput");
var paginationAmount = 2;

function updPage(newPage) {
    if (searchInput.value != "") {
        if (newPage > 0 && newPage <= new Number(lastPage.innerHTML)) {
            curPage.innerText = newPage;
            Promise.all([
                fetch("/templates/event.mst").then(x => x.text()),
                fetch("/api/v1/event").then(x => x.json()),
            ])
                .then(([templateStr, itemsData]) => {
                    let event = new Array();
                    let renderevent = new Array();
                    for (let i = 0; i < itemsData.length; i++) {
                        if (itemsData[i].name.toLowerCase().indexOf(searchInput.value.toLowerCase()) > -1) {
                            event.push(itemsData[i]);
                        }
                    }

                    for (let i = (new Number(curPage.innerHTML) - 1) * paginationAmount; i < (new Number(curPage.innerHTML) - 1) * paginationAmount + paginationAmount && i < event.length; i++) {
                        if (event[i].name.toLowerCase().indexOf(searchInput.value.toLowerCase()) > -1) {
                            renderevent.push(event[i]);
                        }
                    }
                    const dataObject = { event : renderevent }
                    const renderedHtmlStr = Mustache.render(templateStr, dataObject);
                    return renderedHtmlStr;
                })
                .then(htmlStr => {
                    const appEl = document.getElementById('app');
                    appEl.innerHTML = htmlStr;
                })
                .catch(err => console.log(err))
        }
    }
    else 
    {
        if (newPage > 0 && newPage <= new Number(lastPage.innerHTML)) {
            curPage.innerText = newPage;
            Promise.all([
                fetch("/templates/event.mst").then(x => x.text()),
                fetch("/api/v1/events").then(x => x.json()),
            ])
                .then(([templateStr, itemsData]) => {
                    let itemsData2 = new Array();
                    for (let i = (new Number(curPage.innerHTML) - 1) * paginationAmount; i < (new Number(curPage.innerHTML) - 1) * paginationAmount + paginationAmount && i < itemsData.length; i++) {
                        itemsData2.push(itemsData[i]);
                    }
                    const dataObject = { event: itemsData2 }
                    const renderedHtmlStr = Mustache.render(templateStr, dataObject);
                    return renderedHtmlStr;
                })
                .then(htmlStr => {
                    const appEl = document.getElementById('app');
                    appEl.innerHTML = htmlStr;
                })
                .catch(err => console.log(err))
        }
    }
}

function search(str) {
    Promise.all([
        fetch("/templates/event.mst").then(x => x.text()),
        fetch("/api/v1/events").then(x => x.json()),
    ])
        .then(([templateStr, itemsData]) => {
            let event = new Array();
            let renderevent = new Array();
            for (let i = 0; i < itemsData.length; i++) {
                if (itemsData[i].name.toLowerCase().indexOf(str.toLowerCase()) > -1) {
                    event.push(itemsData[i]);
                    if (renderevent.length < paginationAmount)
                        renderevent.push(itemsData[i]);
                }
            }
            lastPage.innerHTML = Math.ceil(event.length / paginationAmount);
            if (new Number(lastPage.innerHTML) == 0)
                lastPage.innerHTML = 1;
            curPage.innerHTML = 1;

            const dataObject = { event: renderevent }
            const renderedHtmlStr = Mustache.render(templateStr, dataObject);
            return renderedHtmlStr;
        })
        .then(htmlStr => {
            const appEl = document.getElementById('app');
            appEl.innerHTML = htmlStr;
        })
        .catch(err => console.log(err))
}
function clearSearch() {
    searchInput.value = "";
    search("");
}
var lb = document.getElementById("leftButton1");
lb.addEventListener("click", () => updPage(new Number(curPage.innerHTML) - 1));
var rb = document.getElementById("rightButton1");
rb.addEventListener("click", () => updPage(new Number(curPage.innerHTML) + 1));
var cb = document.getElementById("clearButton");
cb.addEventListener("click", () => clearSearch());