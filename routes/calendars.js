const express = require('express');
const router = express.Router();
const eventsMod = require("../models/event");
const calendarsMod = require("../models/calendar");
let multer = require("multer");
const cloudinary = require("cloudinary");
const config = require("../config");
let storage = multer.diskStorage({});

let upload = multer({ storage: storage });

cloudinary.config({
  cloud_name: config.cloud_name,
  api_key: config.api_key,
  api_secret: config.api_secret
});

router.get("/calendars", isLoggedIn, function(req, res) {
    calendarsMod
      .getAll()
      .then(calendars => {
        let userLogin;
            let admin = false;
            if (req.user) {
                userLogin = req.user.login;
                if (req.user.role === 1)
                    admin = true;
            }
        res.render("calendars", { id: req.user._id, calendars: calendars, user: req.user, userLogin: userLogin, admin: admin });
      })
      .catch(err => res.status(500).send(err.toString()));
  });
  
  router.get("/calendars/:id", isLoggedIn, function(req, res) {
    calendarsMod
      .getById(req.params.id)
      .then(calendar => {
        let userLogin;
        let admin = false;
        if (req.user) {
            userLogin = req.user.login;
            if (req.user.role === 1)
                admin = true;
        }
        if (typeof calendar === "undefined") {
          res.status(404).send(`calendar with id ${req.params.id} not found`);
        } else {
          eventsMod
            .findAllbyId(calendar.eventsId)
            .then(events =>
              res.render("calendar", {id: req.user._id, calendar: calendar, events: events, user: req.user, userLogin: userLogin, admin: admin })
            )
            .catch(err => res.status(500).send(err.toString()));
        }
      })
      .catch(err => res.status(500).send(err.toString()));
  });
  
  router.get("/calendar/new", isLoggedIn, function(req, res) {
    let userLogin;
        let admin = false;
        if (req.user) {
            userLogin = req.user.login;
            if (req.user.role === 1)
                admin = true;
        }
    eventsMod
      .getAll()
      .then(events => res.render("calendaradd", {id: req.user._id, events: events, user: req.user,userLogin: userLogin, admin: admin }))
      .catch(err => res.status(500).send(err.toString()));
  });
  
  router.post("/calendar/new", upload.single("imgUrl"), function(req, res) {
    const date = new Date();
    date.toISOString();
    const file = req.file;
    // console.log(data);
    // });
    if (!file) {
      calendarsMod
        .insert(req.body.name, req.body.eve, null)
        .then(calendar => res.redirect("/calendars/" + calendar.id))
        .catch(err => res.status(500).send(err.toString()));
    } else {
      cloudinary.v2.uploader.upload(file.path, function(err, data) {
        calendarsMod
          .insert(req.body.name, req.body.eve, data.url)
          .then(calendar => res.redirect("/calendars/" + calendar.id))
          .catch(err => res.status(500).send(err.toString()));
      });
    }
  });
  
  router.get("/calendar/update/:id", isLoggedIn, function(req, res) {
    calendarsMod
      .getById(req.params.id)
      .then(calendar => {
        let userLogin;
        let admin = false;
        if (req.user) {
            userLogin = req.user.login;
            if (req.user.role === 1)
                admin = true;
        }
        if (typeof calendar === "undefined") {
          res.status(404).send(`calendar with id ${req.params.id} not found`);
        } else {
          eventsMod
            .getAll()
            .then(events =>
              res.render("calendarupd", { calendar: calendar, events: events, user: req.user, userLogin: userLogin, admin: admin, id: req.user._id})
            )
            .catch(err => res.status(500).send(err.toString()));
        }
      })
      .catch(err => res.status(500).send(err.toString()));
  });
  
  router.post("/calendar/update/:id", upload.single("imgUrl"), function(req, res) {
    const date = new Date();
    date.toISOString();
    const file = req.file;
    if (!file) {
      calendarsMod
        .update(req.params.id, req.body.name, req.body.eve, null)
        .then(calendar => res.redirect("/calendars/" + calendar.id))
        .catch(err => res.status(500).send(err.toString()));
    } else {
      cloudinary.v2.uploader.upload(file.path, function(err, data) {
        calendarsMod
          .update(req.params.id, req.body.name, req.body.eve, data.url)
          .then(calendar => res.redirect("/calendars/" + calendar.id))
          .catch(err => res.status(500).send(err.toString()));
      });
    }
  });
  
  router.post("/calendar/delete/:id",isLoggedIn, function(req, res) {
    calendarsMod
      .delete(req.params.id)
      .then(() => res.redirect("/calendars"))
      .catch(err => res.status(500).send(err.toString()));
  });
 
 
 
  function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/auth/login');
}
module.exports = router;