const express = require("express");
const router = express.Router();
const eventsMod = require("../models/event");
const userMod = require("../models/user");
const cloudinary = require("cloudinary");
const config = require("../config");

let multer = require("multer");

let storage = multer.diskStorage({});

let upload = multer({ storage: storage });

cloudinary.config({
  cloud_name: config.cloud_name,
  api_key: config.api_key,
  api_secret: config.api_secret
});

router.get("/events?page=:id", isLoggedIn, function(req, res) {
  let userLogin;
  let admin = false;
  if (req.user) {
    userLogin = req.user.login;
    if (req.user.role === 1) admin = true;
  }
  const current = parseInt(req.params.id);
  let pageNumber;
  let next = current;
  let past = 1;
  eventsMod.Count().then(number => {
    pageNumber = Math.ceil(number / 2);
    if (pageNumber === 0) {
      pageNumber += 1;
    }
    if (current > 1) {
      past = current - 1;
    }
    if (pageNumber > current) {
      next = current + 1;
    }
  });
  if (req.query.search === undefined || req.query.search === "") {
    eventsMod
      .Paginated((current - 1) * 2, 2)
      .then(events => {
        res.render("events", {
          events: events,
          page: current,
          next: next,
          past: past,
          pageNumber: pageNumber,
          user: req.user,
          userLogin: userLogin,
          admin: admin
        });
      })
      .catch(err => {
        console.log("Error: " + err);
        res.redirect("/");
      });
  } else {
    eventsMod
      .find(req.query.search)
      .then(events => {
        // console.log(events);
        res.render("events", {
          events: events,
          page: 1,
          next: next,
          past: past,
          pageNumber: 1,
          user: req.user,
          userLogin: userLogin,
          admin: admin
        });
      })
      .catch(err => {
        console.log("Error: " + err);
        res.redirect("/");
      });
  }
});

router.get("/events/new", isLoggedIn, function(req, res) {
  let userLogin;
  let admin = false;
  if (req.user) {
    userLogin = req.user.login;
    if (req.user.role === 1) admin = true;
  }
  res.render("eventadd", { user: req.user, userLogin: userLogin, admin: admin });
});

router.post("/events/new", upload.single("imgUrl"), (req, res) => {
  const date = new Date();
  date.toISOString();
  const file = req.file;
  if (!file) {
    eventsMod
      .insert(
        req.body.name,
        req.body.task,
        req.body.motivation,
        req.body.difficulty,
        req.body.deadline,
        null
      )
      .then(event => res.redirect("/events/" + event.id))
      .catch(err => res.status(500).send(err.toString()));
  } else {
    cloudinary.v2.uploader.upload(file.path, function(err, data) {
      eventsMod
        .insert(
          req.body.name,
          req.body.task,
          req.body.motivation,
          req.body.difficulty,
          req.body.deadline,
          data.url
        )
        .then(event => res.redirect("/events/" + event.id))
        .catch(err => res.status(500).send(err.toString()));
    });
  }
});

router.get("/events/:id", isLoggedIn, function(req, res) {
  let userLogin;
  let admin = false;
  if (req.user) {
    userLogin = req.user.login;
    if (req.user.role === 1) admin = true;
  }
  eventsMod
    .getById(req.params.id)
    .then(event => res.render("event", { event: event, user: req.user, userLogin: userLogin, admin: admin }))
    .catch(err => res.status(500).send(err.toString()));
});

router.post("/events/:id", (req, res) => {
  eventsMod
    .delete(req.params.id)
    .then(() => res.redirect("/eventspage=1"))
    .catch(err => res.status(500).send(err.toString()));
});

function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) return next();

  res.redirect("/auth/login");
}

module.exports = router;
