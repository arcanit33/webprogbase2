const express = require("express");
var mongoose = require("mongoose");
const router = express.Router();
const userMod = require("../models/user");
const calendarMod = require("../models/calendar");
const eventMod = require("../models/event");
let multer = require("multer");
const cloudinary = require("cloudinary");
const config = require("../config");
const passport = require("../app");

var storage = multer.diskStorage({});
var upload = multer({ storage: storage });

cloudinary.config({
  cloud_name: config.cloud_name,
  api_key: config.api_key,
  api_secret: config.api_secret
});

router.get("/me", passport.authenticate("basic", { session: false }), function(
  req,
  res
) {
  res.status(200).json(req.user);
});

router.get("/", function(req, res) {
  res.status(200).json("Empty json");
});

router.get(
  "/users",
  passport.authenticate("basic", { session: false }),
  function(req, res) {
    if (req.user.role !== 1) return res.status(403).send("Forbidden");

    const Curent = parseInt(req.query.page);
    const Amount = 1;
    userMod
      .Paginated((Curent - 1) * Amount, Amount)
      .then(users => {
        res.status(200).json(users);
      })
      .catch(err => {
        // console.log('silno-tupoi');
        res.status(400).send(err);
      });
  }
);

router.get(
  "/users/:id",
  passport.authenticate("basic", { session: false }),
  function(req, res) {
    const userId = req.params.id;

    userMod
      .findById(userId)
      .then(user => res.status(200).json(user))
      .catch(err => res.status(500).send(err));
  }
);

router.post("/users", function(req, res) {
  console.log(req.body);
  userMod
    .findOne({ login: req.body.login })
    .then(user => {
      if (user && !user.isDisabled) res.send("Email already taken");
      else if (req.body.password !== req.body.password2)
        res.send("Passwords not match");
      else if (req.body.password.length < 5) res.send("Password length < 5");
      else if (req.body.login.length < 4) res.send("Login length < 4");
      else if (req.body.fullname.length < 2) res.send("Fullname length < 2");
      else {
        let newUser = new userMod();
        console.log("1");
        newUser._id = new mongoose.Types.ObjectId();
        newUser.login = req.body.login;
        newUser.password = newUser.generateHash(req.body.password);
        newUser.fullname = req.body.fullname;
        newUser.biography = "";
        newUser.role = 0;
        newUser.registeredAt = new Date().toISOString();
        newUser.avaUrl =
          "https://res.cloudinary.com/hmgldm3w6/image/upload/v1574987203/qhdzojllhx0ip2shjmav.gif";
        newUser.isDisabled = false;
        newUser.eventsId = [];
        newUser.calendarsId = [];

        return userMod.insert(newUser);
      }
    })
    .then(x => res.status(200).send(x))
    .catch(err => {
      res.status(400).send(err), console.log(err);
    });
});

router.put(
  "/users/:id",
  passport.authenticate("basic", { session: false }),
  function(req, res) {
    const userId = req.params.id;

    if (userId != req.user._id && req.user.role != 1)
      return res.status(403).send("Forbidden");

    userMod
      .findById(userId)
      .then(user => {
        let login = user.login;
        let eventsId = user.eventsId;
        let calendarsId = user.calendarsId;
        let fullname = user.fullname;
        let biography = user.biography;
        let role = user.role;
        let isDisabled = user.isDisabled;

        if (req.body.login) login = req.body.login;
        if (req.body.eventsId) eventsId = req.body.eventsId;
        if (req.body.calendarsId) calendarsId = req.body.calendarsId;
        if (req.body.fullname) fullname = req.body.fullname;
        if (req.body.biography) biography = req.body.biography;
        if (req.body.role) role = req.body.role;
        if (req.body.isDisabled) isDisabled = req.body.isDisabled;

        return userMod.update(userId, {
          login: login,
          eventsId: eventsId,
          calendarsId: calendarsId,
          fullname: fullname,
          biography: biography,
          role: role,
          isDisabled: isDisabled
        });
      })
      .then(x => res.status(200).json(x))
      .catch(err => res.status(400).send(err));
  }
);

router.delete(
  "/users/:id",
  passport.authenticate("basic", { session: false }),
  function(req, res) {
    const userId = req.params.id;
    console.log(req.user._id);
    console.log(req.params.id);
    if (userId != req.user._id && req.user.role != 1)
      return res.status(403).send("Forbidden");

    userMod
      .findByIdAndDelete(userId)
      .then(x => res.status(200).send("succes"))
      .catch(err => res.status(400).send(err));
  }
);

///////////////////////
/////////////////////

router.get(
  "/events",
  passport.authenticate("basic", { session: false }),
  function(req, res) {
    if (req.user.role !== 1) return res.status(403).send("Forbidden");
      eventMod.getAll()
            .then(events => {
        res.status(200).json(events);
      })
      .catch(err => {
        // console.log('silno-tupoi');
        res.status(400).send(err);
      });

    // const Curent = parseInt(req.query.page);
    // const Amount = 1;
    // eventMod
    //   .Paginated((Curent - 1) * Amount, Amount)
    //   .then(events => {
    //     res.status(200).json(events);
    //   })
    //   .catch(err => {
    //     // console.log('silno-tupoi');
    //     res.status(400).send(err);
    //   });
  }
);

router.get(
  "/events/:id",
  passport.authenticate("basic", { session: false }),
  function(req, res) {
    const eventId = req.params.id;

    eventMod
      .getById(eventId)
      .then(event => res.status(200).json(event))
      .catch(err => res.status(500).send(err));
  }
);

router.post("/events", passport.authenticate("basic", { session: false }), function(req, res) {

  eventMod
  .insert(
    req.body.name,
    req.body.task,
    req.body.motivation,
    req.body.difficulty,
    req.body.deadline,
    "https://res.cloudinary.com/hmgldm3w6/image/upload/v1574987203/qhdzojllhx0ip2shjmav.gif"

  )
  .then(x => res.status(200).send(x))
    .catch(err => {
      res.status(400).send(err), console.log(err);
    });
});

router.put(
  "/events/:id",
  passport.authenticate("basic", { session: false }),
  function(req, res) {
    const eventId = req.params.id;


    eventMod
      .getById(eventId)
      .then(event => {
        let name = event.name;
        let task = event.task;
        let motivation = event.motivation;
        let difficulty = event.difficulty;
        let deadline = event.deadline;
        let role = user.role;

        if (req.body.name) name = req.body.name;
        if (req.body.task) task = req.body.task;
        if (req.body.motivation) motivation = req.body.motivation;
        if (req.body.difficulty) difficulty = req.body.difficulty;
        if (req.body.deadline) deadline = req.body.deadline;
        if (req.body.role) role = req.body.role;

        return eventMod.update(
           name,
           task,
           motivation,
           difficulty,
           deadline,
           role
        );
      })
      .then(x => res.status(200).json(x))
      .catch(err => res.status(400).send(err));
  }
);

router.delete(
  "/events/:id",
  passport.authenticate("basic", { session: false }),
  function(req, res) {
        eventMod.getById(req.params.id)
        .then(() =>{
        eventMod
          .delete(req.params.id)
          .then(x => res.status(200).send("succes"))
          .catch(err => res.status(400).send(err));
        })
        .catch(err => res.status(400).send(err));


  }
);

////////////////////////////////////////////////////////////////////
/////////////////////////////////////////
/////////////////////////////////////////////////
////////////////////////


router.get(
    "/calendars",
    passport.authenticate("basic", { session: false }),
    function(req, res) {
      if (req.user.role !== 1) return res.status(403).send("Forbidden");
  
      const Curent = parseInt(req.query.page);
      const Amount = 1;
      calendarMod
        .Paginated((Curent - 1) * Amount, Amount)
        .then(calendars => {
          res.status(200).json(calendars);
        })
        .catch(err => {
          // console.log('silno-tupoi');
          res.status(400).send(err);
        });
    }
  );
  
  router.get(
    "/calendars/:id",
    passport.authenticate("basic", { session: false }),
    function(req, res) {
      const calendarId = req.params.id;
  
      calendarMod
        .getById(calendarId)
        .then(calendar => res.status(200).json(calendar))
        .catch(err => res.status(500).send(err));
    }
  );
  
  router.post("/calendars", passport.authenticate("basic", { session: false }), function(req, res) {
  
    calendarMod
    .insert(
      req.body.name,
      [],
      "https://res.cloudinary.com/hmgldm3w6/image/upload/v1574987203/qhdzojllhx0ip2shjmav.gif"
    )
    .then(x => res.status(200).send(x))
      .catch(err => {
        res.status(400).send(err), console.log(err);
      });
  });
  
  router.put(
    "/calendars/:id",
    passport.authenticate("basic", { session: false }),
    function(req, res) {
      const calendarId = req.params.id;
  
    //   if (calendarId != req.calendar._id && req.user.role != 1)
        // return res.status(403).send("Forbidden");
  
      calendarMod
        .getById(calendarId)
        .then(calendar => {
          let name = calendar.name;
          let eventsId = calendar.eventsId;
          let imgUrl = calendar.imgUrl;
  
          if (req.body.name) name = req.body.name;
          if (req.body.eventsId) eventsId = req.body;
          if (req.body.imgUrl) imgUrl =       "https://res.cloudinary.com/hmgldm3w6/image/upload/v1574987203/qhdzojllhx0ip2shjmav.gif"
          ;
  
          return calendarMod.update(calendarId, name,eventsId,imgUrl);
        })
        .then(x => res.status(200).json(x))
        .catch(err => res.status(400).send(err));
    }
  );
  
  router.delete(
    "/calendars/:id",
    passport.authenticate("basic", { session: false }),
    function(req, res) {
          calendarMod.getById(req.params.id)
          .then(() =>{
          calendarMod
            .delete(req.params.id)
            .then(x => res.status(200).send("succes"))
            .catch(err => res.status(400).send(err));
          })
          .catch(err => res.status(400).send(err));
  
  
    }
  );
  



///////////////////////////////
//////////////////////////
/////////////////////////






module.exports = router;
