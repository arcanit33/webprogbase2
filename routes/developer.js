const express = require('express');
const router = express.Router();
const passport = require('../app');

router.get('/', passport.authenticate('basic', { session: false }), function (req, res) {
    if (req.user.role != 1)
        return res.status(403).send("Forbidden");
    res.status(200).render('developers');
})

module.exports = router;