const express = require('express');
const router = express.Router();

router.get('/', function(req, res) {
    let uLog;
    let admin = false;
    if (req.user) {
        uLog = req.user.login;
        if (req.user.role === 1)
            admin = true;
    }
    res.render('index', { user: req.user, login: uLog, admin: admin });
});

module.exports = router;
