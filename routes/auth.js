let multer = require("multer");
const cloudinary = require('cloudinary');
const config = require('../config');
// const pmongo = require('promised-mongo');
var storage = multer.diskStorage({})
var upload = multer({ storage: storage })

const User = require('../models/user');


cloudinary.config({
    cloud_name: config.cloud_name,
    api_key: config.api_key,
    api_secret: config.api_secret
});

module.exports = function (app, passport) {

    app.post('/auth/signup', passport.authenticate('local-signup', {
        failureRedirect: '/auth/signup', 
        failureFlash: true 
    }), (req, res) => {
        res.redirect(`/user/${req.user._id}`);
    });

    app.post('/auth/login', passport.authenticate('local-login', {
        failureRedirect: '/auth/login', 
        failureFlash: true // allow flash messages
    }), (req, res) => {
        res.redirect(`/user/${req.user._id}`);  
    });

    app.get('/auth/signup', function (req, res) {
        let userLogin;
        let admin = false;
        if (req.user) {
            userLogin = req.user.login;
            if (req.user.role === 1)
                admin = true;
        }
        res.render('signup', {
            error: req.flash('signupMessage'),
            title: "Sign up",
            userLogin: userLogin,
            admin: admin,
        });
    });

    app.get('/auth/login', function (req, res) {
        let userLogin;
        let admin = false;
        if (req.user) {
            res.redirect('/user/' + req.user._id);
        }
        else {
            res.render('login', {
                error: req.flash('loginMessage'),
                userLogin: userLogin,
                admin: admin,
            })
        }
    });

    app.get('/user/:id', isLoggedIn, function (req, res) {
        const userId = req.params.id;
        let id = req.user._id;
        let userLogin = req.user.login;
        let userStatus = req.user.status;
       
        let admin = false;
        let status = 'default';
        let change = false;

        if (userId == req.user._id) {
            usersPage = true;
        }

        if (req.user.role === 1) {
            admin = true;
            usersPage = true;
        }

        
        User.findById(id)
        .then(x => {
            
            User.findById(userId)
            .then(user => {
                if (user.role === 1) {   
                    status = 'admin';
                }else{
                    status = 'default';
                };
                // if (userStatus != 1) {
                    // if (id != req.params.id) {
                        //   res.redirect('/error/Forbidden').sendStatus(403); // 'Forbidden'
                         
                // }};
                if (id == userId || x.role == 1) {
                    change = true;
                }
                res.render('user', {
                    info: user,
                    change: change,
                    user: x,
                    admin: admin,
                    userId: userId,
                    status: status
                });
            })
            .catch(err => res.redirect('/error/' + err));
        })
        .catch(err => res.redirect('/error/' + err));
        });
        
        app.post('/user/:id/changerole', checkAdmin, function (req, res) {
        const userId = req.params.id;
        let newRole;
        User.findById(userId)
            .then(user => {
                if (user.role === 1)
                    newRole = 0;
                else
                    newRole = 1;
                return User.updateRole(userId, { 'role': newRole });
           
            })
            .then(x => {
                // console.log("Role changed!");
                if (userId == req.user._id)
                    res.redirect('/');
                else
                    res.redirect('/user/' + userId);
            })
            .catch(err => {
                console.log(err);
                res.redirect('/');
            })

    })

    app.post('/auth/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });

    app.get('/users', checkAdmin, (req, res) => {
        User.getAll()
            .then(users => {
                let userLogin;
                let admin = false;
                if (req.user) {
                    userLogin = req.user.login;
                    if (req.user.role === 1)
                        admin = true;
                }

                res.render('users', {
                    users: users,
                    user: req.user,
                    userLogin: userLogin,
                    admin: admin,
                    pageId: req.user._id
                });
            })
            .catch(err => {
                console.log(err);
                res.redirect('/');
            })
    });

    app.get('/user/update/:id', isLoggedIn,(req, res) => {
        let userId = req.user._id;
        let userStatus = req.user.status;
        
        User.getById(req.params.id)
        .then(user => {
            let userLogin;
                    if (req.user) {
                        userLogin = req.user.login;
                        if (req.user.role === 1)
                            admin = true;
                    }
                    // if (userStatus != 1) {
                        // if (userId != req.params.id) {
                            //   res.redirect('/error/Forbidden').sendStatus(403); // 'Forbidden'
                             
                    // }}
            res.render('userupd',{
                user: user,
                userLogin: userLogin,
                pageId: req.params.id
            })
        })


    });

    app.post('/user/update/:id', isLoggedIn, upload.single("avaUrl"),(req, res) => {
        const date = new Date();
        date.toISOString();
        // console.log(data);
        const file = req.file;
        // console.log(file);
        if (!file) {
          User
            .update(req.params.id, req.body.login, req.body.fullname, null)
            .then(user => res.redirect("/user/" + user._id))
            .catch(err => res.status(500).send(err.toString()));
        } else {
          cloudinary.v2.uploader.upload(file.path, function(err, data) {
            User
              .update(req.params.id, req.body.login, req.body.fullname, data.url)
              .then(user => res.redirect("/user/" + user._id))
              .catch(err => res.status(500).send(err.toString()));
          });
        }
    });

    app.post("/user/delete/:id",isLoggedIn, function(req, res) {
        // let userId = req.user._id;
        // let userStatus = req.user.status;
        // if (userStatus != 1) {
        //     if (userId != req.params.id) {
        //           res.redirect('/error/Forbidden').sendStatus(403); // 'Forbidden'
                 
        // }}
        User
          .delete(req.params.id)
          .then(() => res.redirect("/"))
          .catch(err => res.status(500).send(err.toString()));
      });
    

    function isLoggedIn(req, res, next) {
        if (req.isAuthenticated())
            return next();

        res.redirect('/auth/login');
    }

    function checkAdmin(req, res, next) {
        if (!req.user)
            res.redirect('/auth/login');
        // res.sendStatus(401); // 'Not authorized'
        else if (req.user.role !== 1) {
            // res.sendStatus(403); // 'Forbidden'
            res.redirect('/error/403');
        }
        else next();  // пропускати далі тільки аутентифікованих із роллю 'admin'
    }
}