module.exports = function (app) {

    app.get('/error/:id', function (req, res) {
        let userLogin;
        let admin = false;
        if (req.user) {
            userLogin = req.user.login;
            console.log(userLogin)
            if (req.user.role === 1)
                admin = true;
        }
        res.render('error', {
            user: req.user,
            title: "Error", 
            _id: req.params.id,
            admin: admin, 
            pageId: req.user._id, 
            userLogin: userLogin
        })
    })
}