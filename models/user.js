var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var UserSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    login: String,
    password: String,
    fullname: String,
    biography: String,
    role: Number,
    registeredAt: Date,
    avaUrl: String,
    isDisabled: Boolean,
    eventsId: Array,
    calendarsId: Array,
});

UserSchema.methods.generateHash = function (password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

UserSchema.methods.validPassword = function (password) {
  // console.log(password, this.password);
  return bcrypt.compareSync(password, this.password);
};

const UserModel = mongoose.model("User", UserSchema);

UserModel.getAll = function () {
  return UserModel.find();
};

UserModel.Paginated = function(skip, limit){
    return UserModel.find(null, null, { skip, limit });
};

UserModel.getById = function (id) {
  return UserModel.findById(id);
};

UserModel.getByLogin = function (login) {
  return UserModel.find({ login: login });
};
// UserModel.count = function(){
//   console.log(UserModel.estimatedDocumentCount());
//   return UserModel.estimatedDocumentCount();
// };

UserModel.insert = function (user) {
  return new UserModel(user).save();
};

UserModel.delete = function (id) {
  return UserModel.findByIdAndDelete(id);
};

UserModel.updateRole = function (id, updateInfo) {
  return UserModel.findOneAndUpdate({ _id: id }, updateInfo);
};

UserModel.update = function(id, login, fullname, avaUrl){
  if (avaUrl) {
    return UserModel.findByIdAndUpdate({_id:id}, {login: login, fullname: fullname, avaUrl: avaUrl});      
  }else{
    return UserModel.findByIdAndUpdate({_id:id}, {login: login, fullname: fullname});      

  }
}

module.exports = mongoose.model('User', UserSchema);

// class User {
//   constructor(id, login, role, fullname, registeredAt, avaUrl, isDisabled) {
//     this.id = id;
//     this.login = login;
//     this.role = role;
//     this.fullname = fullname;
//     this.registeredAt = registeredAt;
//     this.avaUrl = avaUrl;
//     this.isDisabled = isDisabled;
//   }
//   static getAll() {
//     return UserModel.find();
//   }
//   static getById(id) {
//     return UserModel.findById(id);
// }
// }
// module.exports = User;
