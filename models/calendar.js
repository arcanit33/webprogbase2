const mongoose = require("mongoose");
// const event = require("./event");

const CalendarSchema = mongoose.Schema({
  name: { type: String, required: true },
  eventsId: {type: Array},
  imgUrl: { type: String, required: true }
});

const CalendarModel = mongoose.model("Calendar", CalendarSchema);

class Calendar {
  constructor(name, eventsId, imgUrl) {
    this.name = name;
    this.eventsId = eventsId;
    this.imgUrl = imgUrl;
  }
  static getAll() {
    return CalendarModel.find();
  }

  static getById(id) {
    return CalendarModel.findById(id);
  }

  static insert(name, eventsId, imgUrl) {
    if (imgUrl) {
      const newCalendar = new CalendarModel({
        name: name,
        eventsId: eventsId,
        imgUrl: imgUrl
      });
      return newCalendar.save();
    } else {
      const newCalendar = new CalendarModel({
        name: name,
        eventsId: eventsId,
        imgUrl: "https://res.cloudinary.com/hmgldm3w6/image/upload/v1574987203/qhdzojllhx0ip2shjmav.gif"
      });
      return newCalendar.save();
    }
  }
  static delete(id) {
    // return EventModel.findByIdAndDelete({_id: id});
    return CalendarModel.findByIdAndDelete(id);

  }

  static Paginated(skip, limit) {
    return CalendarModel.find(null, null, { skip, limit });
  }

  static update(id, name, eventsId, imgUrl){
    if (imgUrl) {
      return CalendarModel.findByIdAndUpdate({_id:id}, {name: name, eventsId: eventsId, imgUrl: imgUrl});      
    }else{
      return CalendarModel.findByIdAndUpdate({_id:id}, {name: name, eventsId: eventsId});      

    }
  }
}

module.exports = Calendar;
