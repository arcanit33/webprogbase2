const mongoose = require("mongoose");

const EventSchema = mongoose.Schema({
  name: { type: String, required: true },
  task: { type: String, required: true },
  motivation: { type: String, default: "W B " },
  difficulty: { type: Number, required: true },
  deadline: { type: String, required: true },
  imgUrl: { type: String}
});

const EventModel = mongoose.model("Event", EventSchema);

class Event {
  constructor( name, task, motivation, difficulty, deadline, imgUrl) {
    this.name = name;
    this.task = task;
    this.motivation = motivation;
    this.difficulty = difficulty;
    this.deadline = deadline;
    this.imgUrl = imgUrl;
  }

  static getAll() {
    return EventModel.find();
  }

  static find(string){
    return EventModel.find({name: string});
  }

  static findAllbyId(string){
    return EventModel.find({_id: string});
  }

  static Paginated(skip, limit) {
    return EventModel.find(null, null, { skip, limit });
  }
  static Count() {
    return EventModel.estimatedDocumentCount();
  }

  static getById(id) {
    return EventModel.findById(id);
  }


  static insert(name, task, motivation, difficulty, deadline, imgUrl) {
    if (imgUrl) {
    const newEvent = new EventModel({
      name: name,
      task: task,
      motivation: motivation,
      difficulty: difficulty,
      deadline: deadline,
      imgUrl: imgUrl
    });
    return newEvent.save();
  } else{
    const newEvent = new EventModel({
      name: name,
      task: task,
      motivation: motivation,
      difficulty: difficulty,
      deadline: deadline,
      imgUrl: "https://res.cloudinary.com/hmgldm3w6/image/upload/v1574987203/qhdzojllhx0ip2shjmav.gif"
    });
    return newEvent.save();
  }
  }
  
  static delete(id) {
    // return EventModel.findByIdAndDelete({_id: id});
    return EventModel.findByIdAndDelete(id);

  }

}
module.exports = Event;
